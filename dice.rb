size, times, _ = gets.chomp.split(/d/i).map(&:to_i)
puts "You totaled %d" % [*1..size].sample(times).reduce(:+)
